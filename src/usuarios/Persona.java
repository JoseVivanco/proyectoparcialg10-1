/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import entidades.*;
import interfaz.*;
import java.util.*;
import java.time.*;

/**
 *
 * @author Brank
 */
public class Persona {

    protected static Integer codigoIncremental = 1;    
    protected Integer codigo;
    protected String nombreCompleto;
    protected String correo;
    protected String contraseña;

    public Persona(String nombreCompleto, String correo, String contraseña) {
        this.codigo = Persona.codigoIncremental++;
        this.nombreCompleto = nombreCompleto;
        this.correo = correo;
        this.contraseña = contraseña;
    }    
    
    public static void registrarUsuario() {
        boolean usuarioExiste = false;
        System.out.print("Ingrese su correo : ");
        String nCorreo = Util.ingresoString().toLowerCase();
        Persona usuario = validarUsuario(nCorreo);
        if (!(usuario == null)) {
            usuarioExiste = true;
        }
        if (!(usuarioExiste)) {
            System.out.print("Ingrese su nombre completo : ");
            String nNombreCompleto = Util.toTitle(Util.ingresoString());
            System.out.print("Ingrese su contraseña : ");
            String nContraseña = Util.ingresoString();
            System.out.print("Ingrese su fecha de nacimiento (DD-MM-AAAA) : ");
            LocalDate nFechaNacimiento = Util.ingresoFecha();
            while (nFechaNacimiento == null){
                System.out.print("Formato inválido. Ingrese la fecha correctamente (DD-MM-AAAA) : ");
                nFechaNacimiento = Util.ingresoFecha();
            }
            System.out.print("Ingrese su ciudad de residencia : ");
            String nCiudadResidencia = Util.toTitle(Util.ingresoString());
            System.out.print("Ingrese el numero de tarjetas que desea ingresar : ");
            String nTarjetas = Util.ingresoString();
            while (!(Util.isNumeric(nTarjetas))) {
                System.out.print("Por favor, Ingrese un numero : ");
                nTarjetas = Util.ingresoString();
            }
            ArrayList<Tarjeta> tarjetas = new ArrayList<>();
            for (Integer i = 0; i < Integer.parseInt(nTarjetas); i += 1) {
                Tarjeta tarjeta = entidades.Tarjeta.nuevaTarjeta();
                tarjetas.add(tarjeta);
            }
            Tarjetahabiente nuevoUsuario = new Tarjetahabiente(nNombreCompleto, nCorreo, nContraseña,
                    nFechaNacimiento, nCiudadResidencia, tarjetas);
            interfaz.Data.usuarios.add(nuevoUsuario);
            System.out.println();
            System.out.println("Usuario agregado con exito");
            Util.continuar();
        } else {
            System.out.println();
            System.out.println("El usuario ya está registrado");
            Util.continuar();
        }
    }    

    public static Menu iniciarSesion() {
        Menu menuPersona = null;
        Persona usuario = null;
        System.out.print("Ingrese su correo : ");
        String correo = Util.ingresoString();
        System.out.print("Ingrese su contraseña : ");
        String contraseña = Util.ingresoString();
        usuario = validarUsuario(correo);
        if (usuario != null) {
            if (validarContraseña(usuario, contraseña)) {
                interfaz.Sistema.usuarioActivo = usuario;
                if (usuario instanceof Administrador) {
                    menuPersona = new MenuAdministrador(MenuAdministrador.añadirOpciones());                    
                }
                if (usuario instanceof Tarjetahabiente) {
                    menuPersona = new MenuTarjetahabiente(MenuTarjetahabiente.añadirOpciones());                    
                }
            } else {
                System.out.println();
                System.out.println("Usuario o contraseña incorrecta.");
                Util.continuar();
            }
        } else {
            System.out.println();
            System.out.println("Usuario o contraseña incorrecta.");
            Util.continuar();
        }
        return menuPersona;
    }

    private static Persona validarUsuario(String usuario) {
        Persona resultado = null;
        for (Persona p : interfaz.Data.usuarios) {
            if (p.getCorreo().equalsIgnoreCase(usuario)) {
                resultado = p;                
                break;
            }
        }
        return resultado;
    }

    private static boolean validarContraseña(Persona usuario, String contraseña) {
        return usuario.getContraseña().equals(contraseña);
    }

    /* GETTERS & SETTERS */
    
    public Integer getCodigo() {
        return codigo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}
